$(document).ready(function(){
    $('.buyer_report_product').on('click', function(){
        var self = $(this);
        $.ajax({
            type: "POST",
            url: $(this).attr('href'),
            success: function(msg) {
                self.fadeOut(150, function(){
                    self.after(msg).remove()
                });
            }
        });
        return false;
    });
});