<?php

namespace Nitra\BuyerReportsBundle\Command;

use Nitra\ExtensionsBundle\Command\NitraContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Nitra\StoreBundle\Lib\Globals;

class ReportPriceDownCommand extends NitraContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('buyer:report:price:down')
            ->setDescription('Report buyers on receipt of products');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        Globals::$container = $this->getContainer();
        $dm         = $this->getDocumentManager();
        $translator = $this->getContainer()->get('translator');
        $buyers     = $dm->getRepository('NitraBuyerBundle:Buyer')->findBy(array(
            'report_products'   => array(
                '$exists'   => true,
                '$ne'       => array(),
            ),
        ));
        $i = 1;
        $sendedPriceDown = 0;
        foreach ($buyers as $buyer) {
            // оповещения о товарах (поступили, подешевели)
            $products = $buyer->getReportProducts();
            foreach ($products as $id => $report) {
                $store      = $dm->find('NitraStoreBundle:Store', $report['storeId']);
                Globals::setStore(array(
                    'id'    => $store->getId(),
                    'host'  => $store->getHost(),
                ));
                $product    = $dm->find('NitraProductBundle:Product', $id);
                if (!$store) {
                    $output->writeln('Ошибка! Магазина с id: "' . $report['storeId'] . '" не найден!');
                } elseif (!$store->getMailingEmail()) {
                    $output->writeln('Ошибка! У магазина "' . $store->getName() . '" должен быть указан e-mail для рассылки!');
                } elseif (!$product) {
                    $output->writeln('Ошибка! Товар с id "' . $id . '" не найден!');
                } elseif ($report['priceDown']) {
                    if (!key_exists($report['storeId'], $product->getStorePrice())) {
                        $output->writeln('Ошибка! У товара "' . $product->getName() . '" не указана цена для магазина "' . $store->getName() . '"');
                    } elseif ($report['price'] > $product->getPrice(false)) {
                        // отправляем оповещение
                        $message = $this->sendEmail(
                            $translator->trans('emails.report_price_down.theme', array(), 'NitraBuyerReportsBundle'),       // тема письма
                            array(
                                $store->getMailingEmail()   => $store->getName()                                            // email и имя магазина
                            ),
                            array(
                                $buyer->getEmail()          => $buyer->getName()                                            // email и имя покупателя
                            ), 
                            'NitraBuyerReportsBundle:EmailTemplates:reportPriceDown.html.twig',                             // шаблон письма
                            array(
                                'product'       => $product,                                                                //
                                'oldPrice'      => $report['price'],                                                        //
                                'newPrice'      => $product->getPrice(false),                                                    //
                                'name'          => $buyer->getName(),                                                       // переменные для шаблона
                                'store'         => $store,                                                                  //
                                'TBworkTime'    => $this->renderTextBlock('work_time_email', $store),                       //
                                'TBcontacts'    => $this->renderTextBlock('contacts_email', $store),                        //
                            )
                        );
                        if ($message) {
                            $sendedPriceDown++;
                            // записывааем цену как старую
                            $products[$id]['oldPrice'] = $products[$id]['price'];
                            // записываем новую цену
                            $products[$id]['price'] = $product->getPrice(false);
                        }
                    }
                }
            }
            $buyer->setReportProducts($products);
            $dm->persist($buyer);
            if ($i % 200 == 0) {
                $dm->flush();
            }
            $i++;
        }
        $output->writeln('Отправлено сообщений о снижении цены: ' . $sendedPriceDown);
        $dm->flush();
    }

    protected function sendEmail($theme, $from, $to, $template, array $atributes = array())
    {
        $mailer = $this->getContainer()->get('mailer');
        $message = \Swift_Message::newInstance()
            ->setSubject($theme)
            ->setFrom($from)
            ->setTo($to)
            ->setCharset('UTF-8')
            ->setContentType('text/html')       
            ->setBody($this->getContainer()->get('templating')->render($template, $atributes));

        return $mailer->send($message);
    }

    protected function renderTextBlock($location, $store)
    {
        $textBlocks = $this->getDocumentManager()->getRepository('NitraTextBlockBundle:TextBlock')->findBy(array(
            'location.$id'  => $location,
            'isActive'      => true,
            'stores.$id'    => new \MongoId($store->getId()),
        ));

        return $this->getContainer()->get('templating')->render("NitraTextBlockBundle:TextBlock:TextBlock.html.twig", array(
            'text_blocks' => $textBlocks,
        ));
    }
}