<?php

namespace Nitra\BuyerReportsBundle\Command;

use Nitra\ExtensionsBundle\Command\NitraContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Nitra\StoreBundle\Lib\Globals;

class ReportReceiptCommand extends NitraContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('buyer:report:receipt')
            ->setDescription('Report buyers on products price cut');
    }
	
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        Globals::$container = $this->getContainer();
        $dm         = $this->getDocumentManager();
        $translator = $this->getContainer()->get('translator');
        $buyers     = $dm->getRepository('NitraBuyerBundle:Buyer')->findBy(array(
            'email'             => 'n.basova@nitralabs.com',
            'report_products'   => array(
                '$exists'   => true,
                '$ne'       => array(),
            ),
        ));
        $i = 1;
        $sendedQuantity = 0;
        foreach($buyers as $buyer) {
            // оповещения о товарах (поступили, подешевели)
            $products = $buyer->getReportProducts();
            foreach ($products as $id => $report) {
                $store      = $dm->find('NitraStoreBundle:Store', $report['storeId']);
                $product    = $dm->find('NitraProductBundle:Product', $id);
                if (!$store) {
                    $output->writeln('Ошибка! Магазина с id: "' . $report['storeId'] . '" не найден!');
                } elseif (!$store->getMailingEmail()) {
                    $output->writeln('Ошибка! У магазина "' . $store->getName() . '" должен быть указан e-mail для рассылки!');
                } elseif (!$product) {
                    $output->writeln('Ошибка! Товар с id "' . $id . '" не найден!');
                } elseif ($report['quantity']) {
                    if ($product->getStock() == 'inStock') {
                        // отправляем оповещение
                        $message = $this->sendEmail(
                            $translator->trans('emails.report_quantity.theme', array(), 'NitraBuyerReportsBundle'),         // тема письма
                            array(
                                $store->getMailingEmail()   => $store->getName()                                            // email и имя магазина
                            ),
                            array(
                                $buyer->getEmail()          => $buyer->getName()                                            // email и имя покупателя
                            ), 
                            'NitraBuyerReportsBundle:EmailTemplates:reportQuantity.html.twig',                              // шаблон письма
                            array(
                                'product'       => $product,                                                                //
                                'name'          => $buyer->getName(),                                                       //
                                'store'         => $store,                                                                  // переменные для шаблона
                                'TBworkTime'    => $this->renderTextBlock('work_time_email', $store),                       //
                                'TBcontacts'    => $this->renderTextBlock('contacts_email', $store),                        //
                            )
                        );
                        if ($message) {
                            $sendedQuantity++;
                            // отменяем оповещение у покупателя
                            $products[$id]['quantity'] = false;
                        }
                    }
                }
            }
            $buyer->setReportProducts($products);
            $dm->persist($buyer);
            if ($i%200 == 0) {
                $dm->flush();
            }
            $i++;
        }
        $output->writeln('Отправлено сообщений о поступлении:   ' . $sendedQuantity);
        $dm->flush();
    }
    
    protected function sendEmail($theme, $from, $to, $template, array $atributes = array())
    {
        $mailer = $this->getContainer()->get('mailer');
        $message = \Swift_Message::newInstance()
            ->setSubject($theme)
            ->setFrom($from)
            ->setTo($to)
            ->setCharset('UTF-8')
            ->setContentType('text/html')       
            ->setBody($this->getContainer()->get('templating')->render($template, $atributes));

        return $mailer->send($message);
    }
    
    protected function renderTextBlock($location, $store)
    {
        $textBlocks = $this->getDocumentManager()->getRepository('NitraTextBlockBundle:TextBlock')->findBy(array(
            'location.$id'  => $location,
            'isActive'      => true,
            'stores.$id'    => new \MongoId($store->getId()),
        ));
        return $this->getContainer()->get('templating')->render("NitraTextBlockBundle:TextBlock:TextBlock.html.twig", array(
            'text_blocks' => $textBlocks,
        ));
    }
}