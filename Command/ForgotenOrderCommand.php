<?php

namespace Nitra\BuyerReportsBundle\Command;

use Nitra\ExtensionsBundle\Command\NitraContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Nitra\StoreBundle\Lib\Globals;

class ForgotenOrderCommand extends NitraContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('buyer:forgoten:order')
            ->setDescription('Report buyers if he forgot to place an order')
            ->addArgument('hours-to-send', InputArgument::OPTIONAL, 'Unactive hours to send email', '3');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dm         = $this->getContainer()->get('doctrine.odm.mongodb.document_manager');
        $translator = $this->getContainer()->get('translator');

        Globals::$container = $this->getContainer();

        $argHours   = $input->getArgument('hours-to-send');

        $buyers = $dm->getRepository('NitraBuyerBundle:Buyer')->findBy(array(
            'lastActivity'              => array(
                '$lte'      => new \MongoDate(strtotime('-' . $argHours . ' hours')),
            ),
            'email'                     => array(
                '$exists'   => true,
            ),
            'ordersData.lastCartData'   => array(
                '$exists'   => true,
            ),
        ));

        $i = 1;
        $sendedForgotenMessages = 0;
        foreach ($buyers as $buyer) {
            $ordersData   = $buyer->getOrdersData();
            $lastCartData = $ordersData['lastCartData'];
            if (!key_exists('sended', $lastCartData) && ((key_exists('cart', $lastCartData) && $lastCartData['cart']) || (key_exists('sets', $lastCartData) && $lastCartData['sets']))) {
                $store        = $dm->getRepository('NitraStoreBundle:Store')->find($lastCartData['storeId']);
                $cartProducts = key_exists('cart', $lastCartData) ? $lastCartData['cart'] : array();
                $cartSets     = key_exists('sets', $lastCartData) ? $lastCartData['sets'] : array();
                $totalPrice   = 0;

                $products     = array();
                $productIds   = array();
                if ($cartProducts) {
                    foreach ($cartProducts as $cartProductId => $data) {
                        $productIds[] = $cartProductId;
                        $totalPrice += $data['price'] * $data['quantity'];
                    }
                    $products = $dm->getRepository('NitraProductBundle:Product')->findBy(array(
                        '_id'   => array(
                            '$in'   => $productIds,
                        ),
                    ));
                }

                $sets          = array();
                $setsProducts  = array();
                $setsIds       = array();
                $setsDiscounts = array();
                if ($cartSets) {
                    foreach ($cartSets as $cartSetId => $data) {
                        $setsIds[] = $cartSetId;
                    }
                    $sets = $dm->getRepository('NitraSetsBundle:Sets')->findBy(array(
                        '_id'   => array(
                            '$in'   => $setsIds,
                        ),
                    ));
                    foreach ($sets as $set) {
                        $setsProductIds  = array();
                        foreach ($set->getSetEntries() as $entrie) {
                            $setsProductIds[] = $entrie['product_id'];
                            $setsDiscounts[$entrie['product_id']] = $entrie['discount'];
                        }
                        $setsProducts[$set->getId()] = $dm->getRepository('NitraProductBundle:Product')->findBy(array(
                            '_id'   => array(
                                '$in'   => $setsProductIds,
                            ),
                        ));
                    }
                }


                $productImages = array();
                foreach ($products as $product) {
                    if ($product->getImage()) {
                        $productImages[$product->getId()] = $this->getLiipImagePath($product->getImage(), $store);
                    }
                }
                foreach ($sets as $set) {
                    foreach ($setsProducts[$set->getId()] as $entrie) {
                        $totalPrice += $entrie->getStorePrice()[$store->getId()]['price'] * ((100-$setsDiscounts[$entrie->getId()])/100) * $cartSets[$set->getId()]['amount'];
                        if ($entrie->getImage()) {
                            $productImages[$entrie->getId()] = $this->getLiipImagePath($entrie->getImage(), $store);
                        }
                    }
                }

                $message = $this->sendEmail(
                    $translator->trans('emails.forgoten_order.theme', array(), 'NitraBuyerReportsBundle'),              // тема письма
                    array(
                        $store->getMailingEmail() => $store->getName()                                                  // email и имя магазина
                    ),
                    array(
                        $buyer->getEmail() => $buyer->getName()                                                         // email и имя покупателя
                    ), 
                    'NitraBuyerReportsBundle:EmailTemplates:forgotenOrder.html.twig',                                   // шаблон письма
                    array(
                        'cartData'     => $lastCartData,                                                                //
                        'sets'         => $sets,                                                                        //
                        'setsProducts' => $setsProducts,                                                                //
                        'products'     => $products,                                                                    //
                        'buyer'        => $buyer,                                                                       // переменные для шаблона
                        'store'        => $store,                                                                       //
                        'TBworkTime'   => $this->renderTextBlock('work_time_email', $store),                            //
                        'TBcontacts'   => $this->renderTextBlock('contacts_email', $store),                             //
                        'images'       => $productImages,                                                               //
                        'orderForm'    => $this->getOrderForm($store, $buyer),                                          //
                        'totalPrice'   => $totalPrice,                                                                  //
                    )
                );
                if ($message) {
                    $sendedForgotenMessages++;
                    $lastCartData['sended'] = new \MongoDate(strtotime('now'));
                    $ordersData['lastCartData'] = $lastCartData;
                    $buyer->setOrdersData($ordersData);
                }
            }
            if($i%200 == 0) {
                $dm->flush();
            }
            $i++;
        }

        $dm->flush();
        if ($sendedForgotenMessages) {
            $output->writeln('Отправлено уведомлений о забытых заказах: ' . $sendedForgotenMessages);
        } else {
            $output->writeln('Пользователей с забытыми заказами не найдено');
        }
    }

    protected function sendEmail($theme, $from, $to, $template, array $atributes = array())
    {
        $mailer = $this->getContainer()->get('mailer');
//        $transport = \Swift_SmtpTransport::newInstance('localhost', 25);
//        $mailer = \Swift_Mailer::newInstance($transport);
        $message = \Swift_Message::newInstance()
            ->setSubject($theme)
            ->setFrom($from)
            ->setTo($to)
            ->setCharset('UTF-8')
            ->setContentType('text/html')       
            ->setBody($this->getContainer()->get('templating')->render($template, $atributes));

        return $mailer->send($message);
    }

    protected function renderTextBlock($location, $store)
    {
        $repository = $this->getContainer()->get('doctrine.odm.mongodb.document_manager')->getRepository('NitraTextBlockBundle:TextBlock');
        $text_blocks = $repository->findBy(array(
            'location.$id'  => $location,
            '$and' => array(
                array('isActive' => true),
                array('stores.$id' => new \MongoId($store->getId()))
            )
        ));

        return $this->getContainer()->get('templating')->render("NitraTextBlockBundle:TextBlock:TextBlock.html.twig", array(
            'text_blocks' => $text_blocks,
        ));
    }

    protected function getLiipImagePath($imagePath, $store)
    {
        $cacheManager = $this->getContainer()->get('liip_imagine.cache.manager');
        $srcPath = 'http://' . $store->getHost() . $cacheManager->getBrowserPath($imagePath, 'category_thumb');

        return $srcPath;
    }

    protected function getOrderForm($store, $buyer)
    {
        $router = $this->getContainer()->get('router');
        $builder = $this->getContainer()->get('form.factory')->createNamedBuilder('form', 'form', null, array(
            'action'    => 'http://' . $store->getHost() . $router->generate('buyer_forgoten_order'),
        ));

        return $builder
            ->add('buyerId', 'hidden', array(
                'data'               => $buyer->getId(),
            ))
            ->add('send', 'submit', array(
                'label'              => 'emails.forgoten_order.button',
                'translation_domain' => 'NitraBuyerReportsBundle',
                'attr'               => array(
                    'store_host' => $store->getHost(),
                ),
            ))
            ->getForm()->createView();
    }
}