<?php

namespace Nitra\BuyerReportsBundle\Command;

use Nitra\ExtensionsBundle\Command\NitraContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Nitra\StoreBundle\Lib\Globals;

class AlternativeProductsCommand extends NitraContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('buyer:alternative:products')
            ->setDescription('Report buyers if he forgot to place an order. Offer alternative products.')
            ->addArgument('days-to-send', InputArgument::OPTIONAL, 'Days after sending email of forgoten order to send email', '7');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dm         = $this->getContainer()->get('doctrine.odm.mongodb.document_manager');
        $translator = $this->getContainer()->get('translator');

        Globals::$container = $this->getContainer();

        $argDays    = $input->getArgument('days-to-send');
        
        $buyers = $dm->getRepository('NitraBuyerBundle:Buyer')->findBy(array(
            'email'                     => array(
                '$exists'   => true,
            ),
            'ordersData.lastCartData'   => array(
                '$exists'   => true,
            ),
            'ordersData.lastCartData.sended'   => array(
                '$lte'      => new \MongoDate(strtotime('-' . $argDays . ' days')),
            ),
        ));
        $i = 1;
        $sendedAlternativeProductsMessages = 0;
        $notFoundAlternatives              = 0;
        foreach ($buyers as $buyer) {
            $ordersData   = $buyer->getOrdersData();
            $lastCartData = $ordersData['lastCartData'];
            $store        = $dm->getRepository('NitraStoreBundle:Store')->find($lastCartData['storeId']);
            $cartProducts = key_exists('cart', $lastCartData) ? $lastCartData['cart'] : array();
            $totalPrice   = 0;

            $products     = array();
            $productIds   = array();
            if ($cartProducts) {
                foreach ($cartProducts as $cartProductId => $data) {
                    $productIds[] = $cartProductId;
                    $totalPrice += $data['price'] * $data['quantity'];
                }
                $products = $dm->getRepository('NitraProductBundle:Product')->findBy(array(
                    '_id'   => array(
                        '$in'   => $productIds,
                    ),
                ));
            }


            $productImages       = array();
            $productAlternatives = array();
            $productLinks        = array();
            foreach ($products as $product) {
                if ($product->getImage()) {
                    $productImages[$product->getId()] = $this->getLiipImagePath($product->getImage(), $store);
                }
                $alternatives = $dm->getRepository('NitraProductBundle:Product')->getAlternativeProducts($product, $store);
                if ($alternatives) {
                    $alternative = (count($alternatives) > 2) ? array_slice($alternatives, 2) : $alternatives;
                    $productAlternatives[$product->getId()] = $alternative;
                    foreach ($alternative as $altern) {
                        if ($altern->getImage()) {
                            $productImages[$altern->getId()] = $this->getLiipImagePath($altern->getImage(), $store);
                        }
                        $productLinks[$altern->getId()] = $this->getProductLink($altern);
                    }
                } else {
                    $notFoundAlternatives ++;
                }
            }
            
            $message = $this->sendEmail(
                $translator->trans('emails.alternative_products.theme', array(), 'NitraBuyerReportsBundle'),              // тема письма
                array(
                    $store->getMailingEmail() => $store->getName()                                                  // email и имя магазина
                ),
                array(
                    $buyer->getEmail() => $buyer->getName()                                                         // email и имя покупателя
                ), 
                'NitraBuyerReportsBundle:EmailTemplates:alternativeProducts.html.twig',                             // шаблон письма
                array(
                    'productAlternatives'   => $productAlternatives,                                                //
                    'productLinks'          => $productLinks,                                                       //
                    'products'              => $products,                                                           //
                    'buyer'                 => $buyer,                                                              // переменные для шаблона
                    'store'                 => $store,                                                              //
                    'TBworkTime'            => $this->renderTextBlock('work_time_email', $store),                   //
                    'TBcontacts'            => $this->renderTextBlock('contacts_email', $store),                    //
                    'images'                => $productImages,                                                      //
                )
            );
            if ($message) {
                $sendedAlternativeProductsMessages++;
                $lastCartData['sended'] = true;
                $ordersData['lastCartData'] = $lastCartData;
                $buyer->setOrdersData($ordersData);
            }
            if($i%200 == 0) {
                $dm->flush();
            }
            $i++;
        }
        
        $dm->flush();
        if ($sendedAlternativeProductsMessages) {
            $msg = 'Отправлено предложений с альтернативными товарами: ' . $sendedAlternativeProductsMessages;
            if ($notFoundAlternatives) {
                $msg .= '. Для ' . $notFoundAlternatives . ' товаров не было найдено альтернатив';
            }
            $output->writeln($msg);
        } elseif ($notFoundAlternatives) {
            $msg .= 'Найдено ' . count($buyers) . ' пользователя(ей) с заказами, которые были забыты более ' . $argDays . ' дня(ей). Но для товаров из их корзины не было найдено альтернатив (' . $notFoundAlternatives . ')';
        } else {
            $output->writeln('Пользователей с заказами, которые были забыты более ' . $argDays . ' дня(ей), не найдено.');
        }
    }

    protected function sendEmail($theme, $from, $to, $template, array $atributes = array())
    {
        $mailer = $this->getContainer()->get('mailer');
//        $transport = \Swift_SmtpTransport::newInstance('localhost', 25);
//        $mailer = \Swift_Mailer::newInstance($transport);
        $message = \Swift_Message::newInstance()
            ->setSubject($theme)
            ->setFrom($from)
            ->setTo($to)
            ->setCharset('UTF-8')
            ->setContentType('text/html')
            ->setBody($this->getContainer()->get('templating')->render($template, $atributes));

        return $mailer->send($message);
    }

    protected function renderTextBlock($location, $store)
    {
        $repository = $this->getContainer()->get('doctrine.odm.mongodb.document_manager')->getRepository('NitraTextBlockBundle:TextBlock');
        $text_blocks = $repository->findBy(array(
            'location.$id'  => $location,
            '$and' => array(
                array('isActive' => true),
                array('stores.$id' => new \MongoId($store->getId()))
            )
        ));

        return $this->getContainer()->get('templating')->render("NitraTextBlockBundle:TextBlock:TextBlock.html.twig", array(
            'text_blocks' => $text_blocks,
        ));
    }

    protected function getLiipImagePath($imagePath, $store)
    {
        $cacheManager = $this->getContainer()->get('liip_imagine.cache.manager');
        $srcPath = 'http://' . $store->getHost() . $cacheManager->getBrowserPath($imagePath, 'category_thumb');
        
        return $srcPath;
    }

    protected function getProductLink($product)
    {
        $router = $this->getContainer()->get('router');
        $seoUrls = $this->getContainer()->hasParameter('seo_urls') ? $this->getContainer()->getParameter('seo_urls') : array();
        if (key_exists('products', $seoUrls) && $seoUrls['products'] && $product->getFullUrlAliasEn()) {
            return '/' . $product->getFullUrlAliasEn();
        } else {
            return $router->generate('product_page', array(
                'slug'  => $product->getAliasEn(),
            ));
        }
    }
}