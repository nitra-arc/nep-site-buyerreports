<?php

namespace Nitra\BuyerReportsBundle\Command;

use Nitra\ExtensionsBundle\Command\NitraContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Nitra\StoreBundle\Lib\Globals;

class ReviewAfterPurchaseCommand extends NitraContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('buyer:review:after:purchase')
            ->setDescription('Review after the purchase')
            ->addArgument('store-host', InputArgument::OPTIONAL, 'Store hose', 'localhost')
            ->addArgument('day-to-send', InputArgument::OPTIONAL, 'Days to send', '14');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        Globals::$container = $this->getContainer();
        $dm         = $this->getDocumentManager();
        $translator = $this->getContainer()->get('translator');
        $tetradka   = $this->getContainer()->hasParameter('tetradka') ? $this->getContainer()->getParameter('tetradka') : 'localhost';
        
        $daysToSend = $input->getArgument('day-to-send');
        $storeHost  = $input->getArgument('store-host');
        
        $store      = $dm->getRepository('NitraStoreBundle:Store')->findOneByHost($storeHost);
        // если магазин не найден
        if (!$store && $this->getContainer()->hasParameter('store_host')) {
            // ищем по хосту указанному в parameters.yml
            $store  = $dm->getRepository('NitraStoreBundle:Store')->findOneByHost($this->getContainer()->getParameter('store_host'));
        }

        if ($store) {
            if ($store->getMailingEmail()) {
                $buyersRepo = $dm->getRepository('NitraBuyerBundle:Buyer');
                // e-mail - необходимо знать куда отправлять
                // телефон - обязательный при оформлении заказа
                $buyers = $buyersRepo->findBy(array(
                    '$and'   => array(
                        array(
                            'email'         => array(
                                '$exists'       => true,
                                '$ne'           => "",
                            ),
                        ),
                        array(
                            'phone'         => array(
                                '$exists'       => true,
                                '$ne'           => "",
                            ),
                        ),
                    ),
                ));

                $progress = $this->getProgressHelper();
                $progress->start($output, count($buyers));

                $i = 1;
                $sendedReviewPurchase = 0;
                foreach ($buyers as $buyer) {
                    $tBuyer = $buyersRepo->getBuyerFromTetradka($tetradka, $buyer->getTetradkaId(), $buyer->getPhone(), true);
                    if (is_array($tBuyer) && ($tBuyer['status'] == 'success')) {
                        if (!$buyer->getTetradkaId()) {
                            $buyer->setTetradkaId($tBuyer['buyer']['id']);
                        }

                        foreach ($tBuyer['orders_history'] as $completedOrderId => $completedOrder) {
                            // если заказ был завершен более чем $daysToSend дней назад
                            if (strtotime("-". $daysToSend . " days") > strtotime($completedOrder['completedAt']['date'])) {
                                $buyerOrdersData = $buyer->getOrdersData();
                                $reviewAfterPurchase = key_exists('reviewAfterPurchase', $buyerOrdersData) ? $buyerOrdersData['reviewAfterPurchase'] : array();
                                // и если по этому заказу еще не отправляли этому покупателю просьбу с отзывом
                                if (!key_exists($completedOrderId, $reviewAfterPurchase)) {
                                    // формирование массива товаров
                                    $productIds = array();
                                    foreach ($completedOrder['orderEntries'] as $orderEntrie) {
                                        $productIds[] = $orderEntrie['productId'];
                                    }
                                    $products = $dm->getRepository('NitraProductBundle:Product')->findBy(array(
                                        '_id'   => array(
                                            '$in'   => $productIds,
                                        ),
                                    ));
                                    
                                    $reviewForms = array();
                                    $productImages = array();
                                    foreach ($products as $product) {
                                        // формы отзыва
                                        $reviewForms[$product->getId()] = $this->getProductReviewForm($product, $store, $buyer);
                                        if ($product->getImage()) {
                                            $productImages[$product->getId()] = $this->getLiipImagePath($product->getImage(), $store);
                                        }
                                    }

                                    $message = $this->sendEmail(
                                        $translator->trans('emails.review_after_purchase.theme', array(), 'NitraBuyerReportsBundle'),       // тема письма
                                        array(
                                            $store->getMailingEmail() => $store->getName()                                                  // email и имя магазина
                                        ),
                                        array(
                                            $buyer->getEmail() => $buyer->getName()                                                         // email и имя покупателя
                                        ), 
                                        'NitraBuyerReportsBundle:EmailTemplates:reviewAfterPurchase.html.twig',                             // шаблон письма
                                        array(
                                            'review'        => $reviewForms,                                                                //
                                            'products'      => $products,                                                                   //
                                            'buyer'         => $buyer,                                                                      // переменные для шаблона
                                            'store'         => $store,                                                                      //
                                            'TBworkTime'    => $this->renderTextBlock('work_time_email', $store),                           //
                                            'TBcontacts'    => $this->renderTextBlock('contacts_email', $store),                            //
                                        ),
                                        $productImages
                                    );
                                    
                                    if ($message) {
                                        $sendedReviewPurchase++;
                                        // запоминаем, что по этому заказу пользователю уже было отправлено письмо
                                        $reviewAfterPurchase[$completedOrderId] = true;
                                        $buyerOrdersData['reviewAfterPurchase'] = $reviewAfterPurchase;
                                        $buyer->setOrdersData($buyerOrdersData);
                                    }
                                }
                            }
                        }
                        if ($i % 200 == 0) {
                            $dm->flush();
                        }
                        $i++;
                    }
                    $progress->advance();
                }
                $progress->finish();
                $dm->flush();
                if ($sendedReviewPurchase) {
                    $output->writeln('Отправлено предложений оставить отзыв о покупке: ' . $sendedReviewPurchase);
                } else {
                    $output->writeln('Отправлять нечего');
                }
            } else {
                $output->writeln('Ошибка! У магазина "' . $store->getName() . '" должен быть указан e-mail для рассылки!');
            }
        } else {
            $output->writeln('Ошибка! Магазин с хостом "' . $storeHost . '" не найден в базе данных!');
        }
    }

    protected function sendEmail($theme, $from, $to, $template, array $atributes = array(), array $images = array())
    {
        $mailer = $this->getContainer()->get('mailer');
        $message = \Swift_Message::newInstance()
            ->setSubject($theme)
            ->setFrom($from)
            ->setTo($to)
            ->setCharset('UTF-8')
            ->setContentType('text/html');
        $message->setBody($this->getContainer()->get('templating')->render($template, $atributes + array('images' => $images)));

        return $mailer->send($message);
    }

    protected function renderTextBlock($location, $store)
    {
        $repository = $this->getContainer()->get('doctrine.odm.mongodb.document_manager')->getRepository('NitraTextBlockBundle:TextBlock');
        $text_blocks = $repository->findBy(array(
            'location.$id'  => $location,
            '$and' => array(
                array('isActive' => true),
                array('stores.$id' => new \MongoId($store->getId()))
            )
        ));
        return $this->getContainer()->get('templating')->render("NitraTextBlockBundle:TextBlock:TextBlock.html.twig", array(
            'text_blocks' => $text_blocks,
        ));
    }

    protected function getProductReviewForm($product, $store, $buyer)
    {
        $router = $this->getContainer()->get('router');
        $builder = $this->getContainer()->get('form.factory')->createNamedBuilder('product', 'form', null, array(
            'action'    => 'http://' . $store->getHost() . $router->generate('buyer_save_email_review'),
        ));
        return $builder
            ->add('buyerId', 'hidden', array(
                'data'               => $buyer->getId(),
            ))
            ->add('objId', 'hidden', array(
                'data'               => $product->getId(),
            ))
            ->add('objName', 'hidden', array(
                'data'               => $product->getFullNameForSearch(),
            ))
            ->add('message', 'textarea', array(
                'label'              => 'review.comment.label',
                'help'               => 'review.comment.help',
                'translation_domain' => 'NitraReviewBundle'
            ))
            ->add('send', 'submit', array(
                'label'              => 'review.comment.button',
                'translation_domain' => 'NitraReviewBundle',
                'attr'               => array(
                    'store_host' => $store->getHost(),
                ),
            ))
            ->getForm()->createView();
    }

    protected function getLiipImagePath($imagePath, $store)
    {
        $cacheManager = $this->getContainer()->get('liip_imagine.cache.manager');
        $srcPath = 'http://' . $store->getHost() . $cacheManager->getBrowserPath($imagePath, 'category_thumb');
        
        return $srcPath;
    }
}