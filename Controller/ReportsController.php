<?php

namespace Nitra\BuyerReportsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Validator\Constraints;
use Nitra\BuyerBundle\Document\Buyer;
use Nitra\StoreBundle\Lib\Globals;
use Nitra\ReviewBundle\Document\Review;

class ReportsController extends Controller
{
    /**
     * @Route("/save-email-review", name="buyer_save_email_review")
     * @Template("NitraBuyerReportsBundle::emailReviewSendPage.html.twig")
     * @param \Symfony\Component\HttpFoundation\Request $request
     */
    public function saveEmailReview(Request $request)
    {
        $dm = $this->container->get('doctrine.odm.mongodb.document_manager');
        $data = $request->request->all();
        foreach ($data as $key => $formData) {
            $objectId   = $formData['objId'];
            $objectName = $formData['objName'];
            $message    = $formData['message'];
            $buyer = $dm->getRepository('NitraBuyerBundle:Buyer')->find($formData['buyerId']);

            switch ($key) {
                case 'product':
                    $routeName       = 'product_page';
                    $object          = $dm->find('NitraProductBundle:Product', $formData['objId']);
                    $routeParameters = $object ? array('slug' => $object->getAlias()) : array();
                    break;
                default:
                    $routeName       = null;
                    $object          = null;
                    $routeParameters = array();
                    break;
            }

            if ($buyer && $object) {
                $review = new Review();
                $review->setBuyer(              $buyer      );
                $review->setMessage(            $message    );
                $review->setAnswerModerated(    true)       ;
                $review->setObjectId(           $objectId   );
                $review->setReviewObjName(      $objectName );
                $review->setStatus(             true        );
                $review->setModerated(          false       );
                $review->setUtensils(           $key        );
                
                $dm->persist($review);
                $dm->flush();
                
                $url = $this->generateUrl($routeName, $routeParameters);

                return array(
                    'object'    => $object,
                    'url'       => $url,
                );
            }
        }

        return array();
    }
    
    /**
     * @Route("/buyer-forgoten-order", name="buyer_forgoten_order")
     * @Method({"POST"})
     */
    public function forgotenOrderAction(Request $request)
    {
        $post = $request->request->all();
        if (key_exists('form', $post) && key_exists('buyerId', $post['form'])) {
            $buyer = $this->get('doctrine_mongodb.odm.document_manager')->getRepository('NitraBuyerBundle:Buyer')->find($post['form']['buyerId']);
            if ($buyer) {
                $ordersData   = $buyer->getOrdersData();
                $lastCartData = $ordersData['lastCartData'];
                $request      = $this->getRequest();
                $request->getSession()->set('buyer', array(
                    'id'    => $buyer->getId(),
                ));
                if (key_exists('cart', $lastCartData)) {
                    $request->getSession()->set('cart', $lastCartData['cart']);
                }
                if (key_exists('sets', $lastCartData)) {
                    $request->getSession()->set('sets', $lastCartData['sets']);
                }
                return new RedirectResponse($this->get('router')->generate('order_page'));
            }
        }
        
        return new RedirectResponse($this->get('router')->generate('nitra_store_home_index'));
    }
    
    /**
     * @Route("/buyer_form_report_price_down/{productId}/{class}", name="buyer_price_down_report", defaults={"class" = ""})
     * @Template("NitraBuyerReportsBundle:Reports:reportPriceDownButton.html.twig")
     */
    public function reportPriceDownAction($productId, $class)
    {
        $session = $this->getRequest()->getSession();
        if ($session->has('buyer')) {
            $buyer = $this->getBuyerBy(array('_id' => $session->get('buyer')['id']));
            $products = $buyer->getReportProducts();
            if (key_exists($productId, $products)) {
                if ($products[$productId]['priceDown']) {
                    return array();
                }
            }
        }
        return array(
            'class'     => $class,
            'productId' => $productId,
        );
    }

    /**
     * @Route("/buyer_save_report_price_down/{productId}", name="buyer_price_down_report_save")
     */
    public function saveReportPriceDown(Request $request, $productId)
    {
        $session = $this->getRequest()->getSession();
        $dm = $this->container->get('doctrine.odm.mongodb.document_manager');
        $product = $dm->getRepository('NitraProductBundle:Product')->find($productId);
        if ($session->has('buyer')) {
            $buyer = $dm->getRepository('NitraBuyerBundle:Buyer')->find($session->get('buyer')['id']);
            $products = $buyer->getReportProducts();
            if (key_exists($productId, $products)) {
                $products[$productId]['priceDown'] = true;
                $products[$productId]['price'] = $product->getPrice();
                $products[$productId]['storeId'] = Globals::getStore()['id'];
            } else {
                $products[$productId] = array(
                    'priceDown' => true,
                    'quantity'  => false,
                    'price'     => $product->getPrice(),
                    'storeId'   => Globals::getStore()['id'],
                );
            }
            $buyer->setReportProducts($products);
            $dm->persist($buyer);
            $dm->flush();
            return $this->render('NitraBuyerReportsBundle:Reports:reportSuccess.html.twig', array(
                'msg' => $this->get('translator')->trans('success.price_down', array(), "NitraBuyerReportsBundle"),
            ));
        } else {
            $form = $this->getReportProductForm();
            if ($request->isXmlHttpRequest() && count($request->request->all())) {
                $form->bind($request);
                if ($form->isValid()) {
                    $buyer = $this->getBuyerBy(array('email' => $form->get('email')->getData()));
                    if ($buyer) {
                        $products = $buyer->getReportProducts();
                        if (key_exists($productId, $products)) {
                            $products[$productId]['priceDown']  = true;
                            $products[$productId]['price']      = $product->getPrice();
                            $products[$productId]['storeId']    = Globals::getStore()['id'];
                        } else {
                            $products[$productId] = array(
                                'priceDown' => true,
                                'quantity'  => false,
                                'price'     => $product->getPrice(),
                                'storeId'   => Globals::getStore()['id'],
                            );
                        }
                        $buyer->setReportProducts($products);
                        $dm->persist($buyer);
                        $dm->flush();
                    } else {
                        $products = array(
                            $productId => array(
                                'priceDown' => true,
                                'quantity'  => false,
                                'price'     => $product->getPrice(),
                                'storeId'   => Globals::getStore()['id'],
                            ),
                        );
                        $buyer = new Buyer();
                        $buyer->setEmail($form->get('email')->getData());
                        $buyer->setReportProducts($products);
                        $dm->persist($buyer);
                        $dm->flush();
                    }
                    return $this->render('NitraBuyerReportsBundle:Reports:reportSuccess.html.twig', array(
                        'msg' => $this->get('translator')->trans('success.price_down', array(), "NitraBuyerReportsBundle"),
                    ));
                } else {
                    return new Response(
                        $this->get('translator')->trans('errors.email', array(), "NitraBuyerReportsBundle"), 404
                    );
                }
            }
            return $this->render('NitraBuyerReportsBundle:Reports:reportPriceDownForm.html.twig', array(
                'form'      => $form->createView(),
                'productId' => $productId,
            ));
        }
    }

    /**
     * @return \Symfony\Component\Form\Form
     */
    protected function getReportProductForm()
    {
        return $form = $this->createFormBuilder()
            ->add('email', 'email', array(
                'required'              => true,
                'label'                 => 'form.report_products.email.label',
                'translation_domain'    => 'NitraBuyerReportsBundle',
            ))
            ->add('submit', 'submit', array(
                'label'                 => 'form.report_products.submit.label',
                'translation_domain'    => 'NitraBuyerReportsBundle',
            ))
            ->getForm();
    }

    /**
     * @Route("/buyer_form_report_quantity/{productId}/{class}", name="buyer_quantity_report", defaults={"class" = ""})
     * @Template("NitraBuyerReportsBundle:Reports:reportQuantityButton.html.twig")
     */
    public function reportQuantityAction($productId, $class)
    {
        $session = $this->getRequest()->getSession();
        if ($session->has('buyer')) {
            $buyer = $this->getBuyerBy(array('_id' => $session->get('buyer')['id']));
            $products = $buyer->getReportProducts();
            if (key_exists($productId, $products)) {
                if ($products[$productId]['quantity']) {
                    return array();
                }
            }
        }
        return array(
            'class'     => $class,
            'productId' => $productId,
        );
    }

    /**
     * @Route("/buyer_save_report_quantity/{productId}", name="buyer_quantity_report_save")
     */
    public function saveReportQuantity(Request $request, $productId)
    {
        $session = $this->getRequest()->getSession();
        $dm = $this->container->get('doctrine.odm.mongodb.document_manager');
        if ($session->has('buyer')) {
            $buyer = $dm->getRepository('NitraBuyerBundle:Buyer')->find($session->get('buyer')['id']);
            $products = $buyer->getReportProducts();
            if (key_exists($productId, $products)) {
                $products[$productId]['quantity'] = true;
            } else {
                $products[$productId] = array(
                    'priceDown' => false,
                    'quantity'  => true,
                    'storeId'   => Globals::getStore()['id'],
                );
            }
            $buyer->setReportProducts($products);
            $dm->persist($buyer);
            $dm->flush();
            return $this->render('NitraBuyerReportsBundle:Reports:reportSuccess.html.twig', array(
                'msg' => $this->get('translator')->trans('success.quantity', array(), "NitraBuyerReportsBundle"),
            ));
        } else {
            $form = $this->getReportProductForm();
            if ($request->isXmlHttpRequest() && count($request->request->all())) {
                $form->bind($request);
                if ($form->isValid()) {
                    $buyer = $this->getBuyerBy(array('email' => $form->get('email')->getData()));
                    if ($buyer) {
                        $products = $buyer->getReportProducts();
                        if (key_exists($productId, $products)) {
                            $products[$productId]['quantity'] = true;
                        } else {
                            $products[$productId] = array(
                                'priceDown' => false,
                                'quantity'  => true,
                                'storeId'   => Globals::getStore()['id'],
                            );
                        }
                        $buyer->setReportProducts($products);
                    } else {
                        $products = array(
                            $productId => array(
                                'priceDown' => false,
                                'quantity'  => true,
                                'storeId'   => Globals::getStore()['id'],
                            ),
                        );
                        $buyer = new Buyer();
                        $buyer->setEmail($form->get('email')->getData());
                        $buyer->setReportProducts($products);
                    }
                    $dm->persist($buyer);
                    $dm->flush();
                    return new Response($this->get('translator')->trans('success.quantity', array(), "NitraBuyerReportsBundle"));
                } else {
                    return new Response(
                        $this->get('translator')->trans('errors.email', array(), "NitraBuyerReportsBundle"), 404
                    );
                }
            }
            return $this->render('NitraBuyerReportsBundle:Reports:reportQuantityForm.html.twig', array(
                'form'      => $form->createView(),
                'productId' => $productId,
            ));
        }
    }

    /**
     * @Route("/buyer_form_report_new_inform", name="buyer_inform_report")
     * @Template("NitraBuyerReportsBundle:Reports:reportInfoForm.html.twig")
     */
    public function formReportNewInformationAction()
    {
        $form = $this->createFormBuilder(null, array(
            'action'    => $this->generateUrl('buyer_inform_report'),
            'attr'      => array(
                'id'        => 'form_report_new_inform',
            ),
        ))
            ->add('email', 'email', array(
                'required'              => true,
                'label'                 => 'form.report_inform.email.label',
                'attr'                  => array(
                    'placeholder'           => 'form.report_inform.email.placeholder',
                ),
                'constraints'           => array(
                    new Constraints\Email(),
                ),
                'translation_domain'    => 'NitraBuyerReportsBundle',
            ))
            ->add('add', 'submit', array(
                'label'                 => 'form.report_inform.submit.label',
                'translation_domain'    => 'NitraBuyerReportsBundle',
            ))
            ->getForm();

        $msg = '';
        if ($this->getRequest()->isXmlHttpRequest()) {
            $data = $this->getRequest()->request->all();
            $form->handleRequest($this->getRequest());
            if ($form->isValid()) {
                $msg = $this->get('translator')->trans(($this->saveReportNewInformation($data, true) != '') ? 'success.information' : 'errors.save', array(), 'NitraBuyerReportsBundle');
            }
        }

        return array(
            'form'  => $form->createView(),
            'msg'   => $msg,
        );
    }

    /**
     * @Route("/buyer_form_report_new_inform2", name="buyer_inform_report_with_username")
     * @Template("NitraBuyerReportsBundle:Reports:reportInfoForm.html.twig")
     */
    public function formReportNewInformationWithUserNameAction()
    {
        $form = $this->createFormBuilder(null, array(
            'action'    => $this->generateUrl('buyer_inform_report_with_username'),
            'attr'      => array(
                'id'        => 'form_report_new_inform2',
            ),
        ))
            ->add('name', 'text', array(
                'required'              => true,
                'label'                 => 'form.report_inform_user.name.label',
                'attr'                  => array(
                    'placeholder'           => 'form.report_inform_user.name.placeholder',
                ),
                'translation_domain'    => 'NitraBuyerReportsBundle',
            ))->add('email', 'email', array(
                'required'              => true,
                'label'                 => 'form.report_inform_user.email.label',
                'attr'                  => array(
                    'placeholder'           => 'form.report_inform_user.email.placeholder',
                ),
                'constraints'           => array(
                    new Constraints\Email(),
                ),
                'translation_domain'    => 'NitraBuyerReportsBundle',
            ))->add('add', 'submit', array(
                'label'                 => 'form.report_inform_user.submit.label',
                'translation_domain'    => 'NitraBuyerReportsBundle',
            ))
            ->getForm();

        $msg = '';
        if ($this->getRequest()->isXmlHttpRequest()) {
            $data = $this->getRequest()->request->all();
            $form->handleRequest($this->getRequest());
            if ($form->isValid()) {
                $msg = $this->get('translator')->trans(($this->saveReportNewInformation($data, true) != '') ? 'success.information' : 'errors.save', array(), 'NitraBuyerReportsBundle');
            }
        }

        return array(
            'form'  => $form->createView(),
            'msg'   => $msg,
        );
    }

    protected function saveReportNewInformation($data, $reportNI = false)
    {
        $dm = $this->get('doctrine.odm.mongodb.document_manager');

        $_Buyer = $dm->getRepository('NitraBuyerBundle:Buyer')->findOneBy(array(
             'email' => $data['form']['email'],
        ));
        $msg = '';
        if ($_Buyer) {
            if ($_Buyer->getReportNewInformation()) {
                $msg = 'exists';
            } else {
                $msg = 'update';
            }
            $_Buyer->setReportNewInformation($reportNI);
            $dm->persist($_Buyer);
        } else {
            $Buyer = new Buyer();
            $Buyer->setEmail($data['form']['email']);
            if (isset($data['form']['name'])) {
                $Buyer->setName($data['form']['name']);
            }
            $Buyer->setReportNewInformation($reportNI);
            $dm->persist($Buyer);
            $msg = 'save';
        }
        $dm->flush();

        return $msg;
    }

    protected function getBuyerBy(array $array)
    {
        $dm = $this->container->get('doctrine.odm.mongodb.document_manager');
        $buyer = $dm->getRepository('NitraBuyerBundle:Buyer')->findOneBy($array);
        return $buyer;
    }
}